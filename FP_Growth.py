from collections import defaultdict, namedtuple


def find_frequent_itemsets(transactions, minsup):
    itemDict = defaultdict(lambda : 0)

    for tr in transactions:
        for i in tr:
            itemDict[i] += 1

    items = defaultdict()

    for item, support in itemDict.items():
        if support >= minsup:
            items[item] = support

    #print(items.items())

    master = FPTree()

    #O(n*m)
    for tr in transactions:
        tr = list(filter(lambda i: i in items, tr))
        tr.sort(key=lambda i : items[i], reverse = True)
        #print(tr)
        master.add(tr)

    routes = master.get_routes()

    def find_suffix_pattern(tree, suffix):

        for item, nodes in tree.items():
            support = sum(node.count for node in nodes)
            #print(item,support)

            if support>=minsup and item not in suffix:

                alpha = [item] + suffix

                yield (alpha, support)
                #print(alpha, support)

                paths = tree.prefix_path(item)

                cond_tree = create_conditional_tree(paths)

                for (i,s) in find_suffix_pattern(cond_tree, alpha):
                    yield (i,s)

    for (itemset, support) in find_suffix_pattern(master, []):
        yield (itemset, support)


class FPTree(object):

    Route = namedtuple('Route', 'head tail')

    def __init__(self):
        self._root = FPNode(self, None, None)

        self._routes = dict()

    def add(self, transaction):
        ptr = self._root

        for item in transaction:
            tmp = ptr.search(item)

            if tmp:
                tmp.increment(1);
            else:
                tmp = FPNode(self, item)
                ptr.add(tmp)
                self.update_route(tmp)

            ptr = tmp

    def update_route(self, node):

        try:
            route = self._routes[node.item]
            route[1].set_neighbour(node)
            self._routes[node.item] = self.Route(route[0], node)
        except KeyError:
            self._routes[node.item] = self.Route(node, node)

    def nodes(self, item):

        try:
            node = self._routes[item][0]
        except KeyError:
            return

        while node:
            yield node
            #print(item, node.item)
            node = node.neighbour

    def prefix_path(self, item):

        def collect_path(node):
            path = []
            while node and not node.root:
                path.append(node)
                node = node.parent

            path.reverse()
            return path

        prefix_paths = []
        for node in self.nodes(item):
            prefix_paths.append(collect_path(node))

        return prefix_paths

    def items(self):

        for item in self._routes.keys():
            yield (item, self.nodes(item))

    @property
    def root(self):
        return self._root

    def get_routes(self):
        return self._routes


class FPNode(object):

    def __init__(self, tree, item, count=1):

        self._tree = tree
        self._item = item
        self._count = count
        self._parent = None
        self._children = {}
        self._neighbour = None

    def add(self, child):

        if not child.item in self._children:
            self._children[child.item] = child
            child._parent = self

    def search(self, item):

        try:
            return self._children[item]
        except KeyError:
            return None

    def increment(self, incr):
        self._count += incr

    @property
    def tree(self):
        return self._tree

    @property
    def item(self):
        return self._item

    @property
    def count(self):
        return self._count

    @property
    def parent(self):
        return self._parent

    @property
    def children(self):
        return self._children

    @property
    def neighbour(self):
        return self._neighbour

    def set_neighbour(self, node):
        self._neighbour = node

    @property
    def root(self):
        return self.item is None and self.count is None

    @property
    def leaf(self):
        return len(self.children)==0


def create_conditional_tree(paths):

    tree = FPTree()

    for path in paths:

        last_item_count = path[-1].count
        last_item = path[-1].item
        #print(last_item_count)
        ptr = tree.root

        for node in path:

            if node.item != last_item:

                tmp = ptr.search(node.item)

                if not tmp:
                    tmp = FPNode(tree, node.item, last_item_count)
                    ptr.add(tmp)
                    tree.update_route(tmp)
                else:
                    tmp.increment(last_item_count)
                #print(tmp.item, tmp.count)
                ptr = tmp

    return tree

if __name__ == '__main__':

    from optparse import OptionParser
    import csv

    p = OptionParser();

    p.add_option('-s','--minimum-support',dest='minmum_support', type='float')

    p.set_defaults(minsup = 0.1)

    options, args = p.parse_args()

    if(len(args)<1):
        p.error('Please provide path to CSV File')

    transactions = []

    #T.C. = O(n*m), n transactions, max m item in each transction
    with open(args[0]) as record:
        for row in csv.reader(record):
            transaction = []
            for item in row:
                if item:
                    transaction.append(item)
            #print(transaction)
            transactions.append(transaction)

    result = []

    options.minmum_support = options.minmum_support*(len(transactions));

    for itemset, support in find_frequent_itemsets(transactions, options.minmum_support):
        if(len(itemset)>=2):
            result.append((itemset, support/len(transactions)))

    #sorted(result, key = lambda key: len(result[key][0]))

    for i in result:
        print(i)
